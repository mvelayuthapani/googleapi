import json
import os
import time
from datetime import date
import uuid

import requests
from core.bigquery import BigQueryClient
from core.google_oauth import OauthAccessTokenGetter
from core.storage import GoogleCloudStorage

_input_query = 'input_query'
_course_query = 'course_query'
_destination_bucket = 'destination_bucket'
_destination_service_account_json_path = 'destination_service_account_json_path'
_query_service_account = 'query_service_account'
_api = 'api'
_api_service_account = 'api_service_account'
_scope = 'scope'
_batch_size = 'batch_size'
_write_to_gcs = 'write_to_gcs'
_local_path = 'local_path'


class GoogleClassRoom:

    def __init__(self, configuration, load_date, id, token):
        self.token = token
        self.configuration = configuration
        self.error_records = []
        self.output_records = []
        self.load_date = load_date
        self.id = id
        pass

    def load(self, data, type, file_name):
        """
        Loading data to either GCS or Local
        :param data:
        :param type:
        :param file_name:
        :return:
        """
        #print("loading data")
        #print(data)
        if self.configuration[_write_to_gcs]:
            self.load_data_to_gcs(data, type, file_name)
        else:
            self.load_data_to_local(data, type, file_name)
        pass

    def load_data_to_gcs(self, data, type, file_name):
        """
        Loading Data to GCS
        :param data:
        :param type:
        :param file_name:
        :return:
        """
        destination_bucket = self.configuration[_destination_bucket]
        #print(destination_bucket)
        destination_service_account_json_path = self.configuration[_destination_service_account_json_path]
        #print(destination_service_account_json_path)
        storage_client = GoogleCloudStorage(destination_bucket, destination_service_account_json_path)
        partition_date = date.today().strftime("%Y-%m-%d")
        file_name = '{}/LoadDate={}/{}.json'.format(type, partition_date, file_name)
        #print(file_name)
        storage_client.create_blob_raw_data(file_name, data)
        print("written file to gcs file_path : {}".format(file_name))
        pass

    def load_data_to_local(self, data, type, file_name):
        """
        Loading Data to Local file System
        :param data:
        :param type:
        :param file_name:
        :return:
        """
        destination_path = self.configuration[_local_path]
        partition_date = date.today().strftime("%Y-%m-%d")
        if not os.path.isdir('LoadDate={}'.format(partition_date)):
            directory = os.path.join(destination_path, type, 'LoadDate={}'.format(partition_date))
            try:
                os.mkdir(directory)
            except FileExistsError as e:
                pass
        file_name = '{}/LoadDate={}/{}.json'.format(type, partition_date, file_name)
        file_path = os.path.join(destination_path, file_name)
        with open(file_path, 'w', encoding='utf-8') as file:
            file.write(data)
        print("written file to local : {}".format('{}/{}'.format(destination_path, file_name)))

    def transform(self, data):
        """
        Converting List of dictionary to String Json Delimited List
        :param data:
        :return:
        """
        #print('inside transform')
        #print(data)
        json_delimited_data = ''
        for value in data:
            print(value)
            json_delimited_data = json_delimited_data + json.dumps(value) + "\n"
            print(json_delimited_data)
        return json_delimited_data

    def load_errors(self, type):
        """
        Loading Error Response Records
        :param type:
        :return:
        """
        print("error count : {}".format(len(self.error_records)))
        if len(self.error_records) > 0:
            file_name = "error_{}".format(str(uuid.uuid4()))
            self.load(self.transform(self.error_records), type, file_name)
        else:
            print("no errors found for batch : {}".format(self.id))
        pass

    def load_sucess_response(self, type):
        """
        Loading Successful Response Records
        :param type:
        :return:
        """
        #print(self.output_records)
        print("output count : {}".format(len(self.output_records)))
        if len(self.output_records) > 0:
            file_name = "{}_{}".format(type, self.id)
            # print(file_name)
            self.load(self.transform(self.output_records), type, file_name)
        else:
            print("no records for batch : {}".format(self.id))
        pass

    def generate_token(self):
        """
        Generating google oauth JWT token
        :return: token
        """
        scopes = " ".join(self.configuration[_scope])
        token_generator = OauthAccessTokenGetter(private_key_json_file=self.configuration[_api_service_account],
                                                 scope=scopes)
        self.token = token_generator.GetAccessToken()
        return self.token

    def execute_courses(self, data):
        """
        This function will be responsible for creating Course and loading success and failure responses
        :param data:
        :return: error and output records counts
        """
        rec_id = 0
        for record in data:
            self.execute_create_api(record, "course")
            print("executing current batch  record :{}".format(rec_id))
            rec_id = rec_id + 1

        self.load_errors('errors')
        self.load_sucess_response('course')
        return {"error": len(self.error_records), 'output': len(self.output_records)}

    def execute_create_api(self, payload, type, wait_time=1, retry=1):
        """
        This function will be responsible for making POST request
        :param payload:
        :param type:
        :param wait_time:
        :param retry:
        :return:
        """
        headers = {
            'authorization': "Bearer {}".format(self.token),
            'content-type': "application/json",
        }
        response = requests.request("GET", self.configuration[_api], data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            self.output_records.extend([self.apply_course_transformation(response.json())])
        elif response.status_code == 401:
            self.generate_token()
            self.execute_create_api(payload, type)
        elif response.status_code == 429:
            # exponential backoff for Rate Limit Exceed Errors untill 6 retries
            wait_time = wait_time * retry
            print("waiting for {} sec".format(wait_time))
            print("status_code {} error :{} payload  :{}".format(response.status_code, response.text, payload))
            time.sleep(wait_time)
            retry = retry + 1
            if retry == 7:
                raise BrokenPipeError("rate limit exceeded max retry completed : {}".format(response.text))
            self.execute_create_api(payload, type, wait_time, retry)
        else:
            print("status_code {} error :{} payload  :{}".format(response.status_code, response.text, payload))
            self.error_records.extend([{"type": type, "status": response.status_code, "load_date": self.load_date}])

    def chunkify(self):
        """
        creating batches for execution
        :return: list of batches
        """
        data = self.execute_bigquery()
        chunks = []
        n = self.configuration[_batch_size]
        for i in range(0, len(data), n):
            chunks.append(data[i:i + n])
        print("no of batches : {}".format(len(chunks)))
        # print(chunks)
        return chunks

    def execute_bigquery(self):
        """
        Executes query and return results
        :return: output data of query
        """
        bigquery = BigQueryClient(self.configuration[_query_service_account])
        return bigquery.execute_select_query(self.configuration[_input_query])

    def apply_course_transformation(self, param):
        """
        Applying transformation for Course response
        :param param:
        :return: transformed Course response
        """
        print(param)
        data = param
        teacher_folder_id = param['teacherFolder']['id']
        try:
            del data['teacherFolder']
            data['teacherFolderId'] = teacher_folder_id
        except KeyError as e:
            data['teacherFolderId'] = None
        return data

    def apply_user_transformation(self, param):
        """
        Applying transformation for Course response
        :param param:
        :return: transformed Course response
        """
        #print('Inside transformation')
        data = param
        #print(type(data))
        #courseId = str(param['courseId'])
        #user_Id = str(param['userId'])
        profile_Id = str(param['profile']['id'])
        givenName = (param['profile']['name']['givenName'])
        familyName = (param['profile']['name']['familyName'])
        fullName = (param['profile']['name']['fullName'])
        emailAddress = (param['profile']['emailAddress'])
        photoUrl = (param['profile']['photoUrl'])
        #permission = (param['profile']['permissions']['permission'])
        #print(param['permissions']['permission'])
        # teacher_folder_id=param['teacherFolder']['id']
        #print(courseId)
        #print(param['userId'])
        #print(profile_Id)
        try:
             #del data['courseId']
             del data['profile']
             #del data['userid']
             #data['course_Id'] = courseId
             #data['user_Id'] = user_Id
             data['profileId'] = profile_Id
             data['givenName'] = givenName
             data['familyName'] = familyName
             data['fullName'] = fullName
             data['emailAddress'] = emailAddress
             data['photoUrl'] = photoUrl
             #data['permission'] = permission
             #print(data)
        except KeyError as e:
             data['courseId']=None
        return data

    def add_student(self, payload, type, wait_time=1, retry=1):
        headers = {
            'authorization': "Bearer {}".format(self.token),
            'content-type': "application/json",
        }

        # print(payload)
        # print(self.configuration[_api])
        # print(json.dumps(payload))

        bigquery = BigQueryClient(self.configuration[_query_service_account])
        # print(bigquery)
        crs = bigquery.execute_select_query(
            """SELECT distinct Courseid FROM `peaceful-joy-242318.xtesting.Google_User` where UserType = 'Student'""")
        #print(crs)
        # crsj = json.dumps(crs)
        for crsc in range(len(crs)):
            #print(*crs[crsc].values())
            _apif = self.configuration[_api] + str(*crs[crsc].values()) + '/students'
            # print(_apif)
            crid = str(*crs[crsc].values())
            sql = """SELECT distinct profile_id userId  FROM `peaceful-joy-242318.xtesting.Google_User` where UserType = 'Student' and courseid in ( cast (""" + crid + """ as string))"""
            # print(sql)
            usd = bigquery.execute_select_query(sql)
            # self.configuration[_input_query])
            # print(usd)
            for usr in range(len(usd)):
                #usrid = json.dumps(usd[usr])
                #print(usrid)
                response = requests.request("POST", _apif, data=json.dumps(usd[usr]), headers=headers)
                #print(response)
                #print(response.json())
                if response.status_code == 200:
                    #print('before transform')
                    #print(response.json())
                    self.output_records.extend([self.apply_user_transformation(response.json())])
                    #dt = self.output_records.extend([self.apply_user_transformation(response.json())])
                    #print('after transform')
                    #print(dt)
                    print("output count : {}".format(len(self.output_records)))
                    print(self.output_records)
                    if len(self.output_records) > 0:
                        file_name = "{}_{}".format(type, self.id)
                        # print(file_name)
                        self.load(self.transform(self.output_records), type, file_name)
                    else:
                        print("no records for batch : {}".format(self.id))
                    pass
                elif response.status_code == 401:
                    self.generate_token()
                    self.execute_create_api(payload, type)
                elif response.status_code == 429:
                    # exponential backoff for Rate Limit Exceed Errors untill 6 retries
                    wait_time = wait_time * retry
                    print("waiting for {} sec".format(wait_time))
                    print("status_code {} error :{} payload  :{}".format(response.status_code, response.text, payload))
                    time.sleep(wait_time)
                    retry = retry + 1
                    if retry == 7:
                        raise BrokenPipeError("rate limit exceeded max retry completed : {}".format(response.text))
                        self.execute_create_api(payload, type, wait_time, retry)
                else:
                    print("status_code {} error :{} payload  :{}".format(response.status_code, response.text, payload))
                    self.error_records.extend(
                        [{"type": type, "status": response.status_code, "load_date": self.load_date}])


    def add_teacher(self, payload, type, wait_time=1, retry=1):
        headers = {
            'authorization': "Bearer {}".format(self.token),
            'content-type': "application/json",
        }

        # print(payload)
        # print(self.configuration[_api])
        # print(json.dumps(payload))

        bigquery = BigQueryClient(self.configuration[_query_service_account])
        # print(bigquery)
        crs = bigquery.execute_select_query(
            """SELECT distinct Courseid FROM `peaceful-joy-242318.xtesting.Google_User` where UserType = 'Teacher'""")
        #print(crs)
        # crsj = json.dumps(crs)
        for crsc in range(len(crs)):
            print(*crs[crsc].values())
            _apif = self.configuration[_api] + str(*crs[crsc].values()) + '/teachers'
            print(_apif)
            crid = str(*crs[crsc].values())
            sql = """SELECT distinct profile_id userId  FROM `peaceful-joy-242318.xtesting.Google_User` where UserType = 'Teacher' and courseid in ( cast (""" + crid + """ as string))"""
            #print(sql)
            usd = bigquery.execute_select_query(sql)
            # self.configuration[_input_query])
            #print(usd)
            for usr in range(len(usd)):
                usrid = json.dumps(usd[usr])
                print(usrid)
                response = requests.request("POST", _apif, data=json.dumps(usd[usr]), headers=headers)
                #print(response)
                if response.status_code == 200:
                    self.output_records.extend([self.apply_user_transformation(response.json())])
                    print("output count : {}".format(len(self.output_records)))
                    print(self.output_records)
                    if len(self.output_records) > 0:
                        file_name = "{}_{}".format(type, self.id)
                        # print(file_name)
                        self.load(self.transform(self.output_records), type, file_name)
                    else:
                        print("no records for batch : {}".format(self.id))
                    pass
                elif response.status_code == 401:
                    self.generate_token()
                    self.execute_create_api(payload, type)
                elif response.status_code == 429:
                    # exponential backoff for Rate Limit Exceed Errors untill 6 retries
                    wait_time = wait_time * retry
                    print("waiting for {} sec".format(wait_time))
                    print("status_code {} error :{} payload  :{}".format(response.status_code, response.text, payload))
                    time.sleep(wait_time)
                    retry = retry + 1
                    if retry == 7:
                        raise BrokenPipeError("rate limit exceeded max retry completed : {}".format(response.text))
                        self.execute_create_api(payload, type, wait_time, retry)
                else:
                    print("status_code {} error :{} payload  :{}".format(response.status_code, response.text, payload))
                    self.error_records.extend(
                        [{"type": type, "status": response.status_code, "load_date": self.load_date}])
