import json
import os
import time
from datetime import date
import uuid

import requests
from core.bigquery import BigQueryClient
from core.google_oauth import OauthAccessTokenGetter
from core.storage import GoogleCloudStorage

_input_query='input_query'
_destination_bucket='destination_bucket'
_destination_service_account_json_path='destination_service_account_json_path'
_query_service_account='query_service_account'
_api='api'
_api_service_account='api_service_account'
_scope='scope'
_batch_size='batch_size'
_write_to_gcs ='write_to_gcs'
_local_path='local_path'


print(_input_query)
print(_destination_bucket)
print(_destination_service_account_json_path)


def execute_bigquery(self):
    """
    Executes query and return results
    :return: output data of query
    """
    print('insert execute bigquery')
    bigquery = BigQueryClient(self.configuration[_query_service_account])
    return bigquery.execute_select_query(self.configuration[_input_query])