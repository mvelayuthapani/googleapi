import argparse
import json
import os
import sys
import time
from datetime import date
import multiprocessing as mp
from functools import partial
from google_classroom import GoogleClassRoom

config_folder = './config/{}.json'
_parallel_count = 'parallel_count'
_id='id'
_batch='batch'
_token='token'
types = ['student']

# for i in range(len(types)):
#     print(types[i])


def execute_batch(batch_details, configuration, load_date, token, type):
    """
    Executes for a batch of records
    :param batch_details:
    :param configuration:
    :param load_date:
    :param token:
    :param type:
    :return: output counts
    """
    id = batch_details[_id]
    batch = batch_details[_batch]
    google_class_room = GoogleClassRoom(configuration, load_date, id, token)
    #print("executing for batch : {}".format(id))
    #print(batch)
    if type == 'student':
        return google_class_room.add_student(batch,type)
    else:
        raise KeyError("wrong type specified : {}".format(type))
    #print(id)
    #print(batch)

def validate_arguements():

    """
    validating the arguements example command : python app.py --type <type>
    :return: arguements
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--type',
                        dest='type',
                        help='type example :- course')

    known_args, unkown_args = parser.parse_known_args(None)
    print("running for  : {} ".format(known_args.type))

    if known_args.type == None:
        parser.print_help()
        sys.exit(1)
    return str(known_args.type)


type = validate_arguements().lower()

if __name__ == '__main__':

    # getting the required arguements
    type = validate_arguements().lower()

    #starting the process
    start_time = int(round(time.time() * 1000))

    if type not in types:
        raise KeyError("wrong type specified : {} available types : {}".format(type, types))
    config_file_path = os.path.join(config_folder.format(type.lower()))
    #print(config_file_path)

    # reading the configuration from json file
    with open(config_file_path, 'r') as f:
        configuration_file_data = f.read()
    configuration = json.loads(configuration_file_data)
    #print(configuration)
    load_date = print(str(date.today()))

    # creating batches
    google_class_room = GoogleClassRoom(configuration, load_date, 0, '')
    batches = google_class_room.chunkify()
    batches = [{_id: id + 1, _batch: batches[id]} for id in range(len(batches))]

    #print(batches)

    # acquiring required configuration
    output_counts = []
    token = configuration[_token]
    parallel_count = configuration[_parallel_count]
    #print(parallel_count)

    # parallelizing operation if parallel count is greater than 1
    if parallel_count > 1:
        pool = mp.Pool(parallel_count)
        output_counts = pool.map(
            partial(execute_batch, configuration=configuration, load_date=load_date, token=token, type=type),
            batches)
        pool.close()
    else:
        for id in range(len(batches)):
            output_counts.extend([execute_batch(batches[id], configuration, load_date, token, type)])

    # ending the execution
    end_time = int(round(time.time() * 1000))
    duration = str(int(round((end_time - start_time) / 1000))) + " secs"
    print("time taken to process : {} sec".format(duration))