import json

from google.cloud import bigquery
from google.oauth2.service_account import Credentials

class BigQueryClient(object):

    def __init__(self,service_account_path):
        print("service account : {} ".format(service_account_path))
        self.service_account_path=service_account_path
        pass

    def get_client(self):
        with open(self.service_account_path,'r') as service_account_file:
            service_account_json=service_account_file.read()
        credentials_json=dict(json.loads(service_account_json))
        credentials = Credentials.from_service_account_info(credentials_json)
        return bigquery.Client(credentials=credentials, project=credentials_json['project_id'])

    def execute_select_query(self,query):
        query_job = self.get_client().query(query)
        result= query_job.result()
        row_list=[]
        for row in result:
            row_list.extend([dict(row)])
        return row_list

    def execute_update_query(self,query):
        query_job = self.get_client().query(query)
        result = query_job.result()
        return result