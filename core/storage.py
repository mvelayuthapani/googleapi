from google.cloud import storage


class GoogleCloudStorage(object):

    def __init__(self, bucket_name, service_account_cred_path):
        """
        Object Initialization, Constructs minimal required parameters.
        :param bucket_name: Name of the bucket the data has to be loaded.
        :param service_account_cred_path: Path to read Google service account credentials.
        """
        self.credentials = service_account_cred_path
        self.client = self.get_connection()
        self.bucket = self.client.get_bucket(bucket_name)

    def get_connection(self, credentials=None):
        """
        Function that establish connection to GCS bucket.
        :param credentials: client credentials json file path parsed as input to the program
        :return: Google Client Storage object.
        """
        if credentials is None:
            credentials = self.credentials
        return storage.Client.from_service_account_json(credentials)

    def create_blob(self, name, data=None):
        """
        Function that create Blob inside bucket with the given name.
        :param name: Name of the Blob to be created.
        :param data: This is an optional parameter, it behaves differently for each of parameter given below.
        1) None: This will create empty blob with given name. Mostly this will be used on blob truncation.
        2) Not None: This will create json file inside the given blob name
        """
        try:
            if not name.endswith(".json"):
                name = name + "/"
            blob = self.bucket.blob(blob_name=name)
            print("file created with name :{} ".format(name))
            if data is not None:
                print('uploading data .....!')
                blob.upload_from_string('\n'.join(data), content_type="application/json")
            else:
                blob.upload_from_string('')
        except Exception as e:
            raise e

    def delete_blob(self,prefix='/'):
        """
        Function that deletes files in the bucket
        :param prefix: prefix in which the files to be listed
        """

        try :
            bucket = self.client.bucket(self.bucket.name)
            bucket.delete_blobs(blobs=bucket.list_blobs(prefix=prefix))
            print("Blob {} deleted.".format(prefix))
        except Exception as e:
            raise e


    def list_blob(self,prefix='/'):

        """
        Function that list files in the bucket
        :param prefix: prefix in which the files to be listed
        :returns blob_list   type: list
        """

        try:
            blob_list=[]
            blobs = self.client.list_blobs(self.bucket.name, prefix=prefix)
            for blob in blobs:
                blob_list.extend([blob.name])
            return blob_list
        except Exception as e:
            raise e

    def list_blob_pattern(self,pattern, prefix='/'):
        pattern_list=[]
        blob_list=self.list_blob(prefix)
        for blob in blob_list:
            if blob.find(pattern) !=-1:
                blobs=blob.split('/')
                pattern_list.extend([blobs[len(blobs)-1]])
        return pattern_list

    def create_blob_raw_data(self, name, data=None):
        """
        Function that create Blob inside bucket with the given name.
        :param name: Name of the Blob to be created.
        :param data: This is an optional parameter, it behaves differently for each of parameter given below.
        1) None: This will create empty blob with given name. Mostly this will be used on blob truncation.
        2) Not None: This will create json file inside the given blob name
        """
        try:
            if not name.endswith(".json"):
                name = name + "/"
            blob = self.bucket.blob(blob_name=name)
            print("file created with name :{} ".format(name))
            if data is not None:
                print('uploading data .....!')
                blob.upload_from_string(data, content_type="application/json")
            else:
                blob.upload_from_string('')
        except Exception as e:
            raise e







