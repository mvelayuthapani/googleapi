import os


def get_filenames_in_directory(path='/'):
    file_names = os.listdir(path)
    return file_names

def get_filenames_in_directory_with_pattern(pattern,path='/'):
    pattern_files=[]
    file_names=get_filenames_in_directory(path)
    for file_name in file_names:
        if file_name.find(pattern) != -1:
            pattern_files.extend([file_name])
    return pattern_files
