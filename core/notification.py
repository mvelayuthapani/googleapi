import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from json2html import *

from core.crypto import CryptoAES

crypt_aes = CryptoAES()


def send_email(notification_config, metrics_data):
    sender_email = notification_config['notification_mail']
    receiver_email = notification_config['notification_receivers']
    password = crypt_aes.decrypt(notification_config['notification_mail_password'])
    port = notification_config['notification_smtp_port']
    smtp_server = notification_config['notification_smtp_server']

    message = MIMEMultipart("alternative")
    message["Subject"] = "Pipeline {} - {} ".format(metrics_data['pipeline_name'], metrics_data['pipeline_state'])
    message["From"] = sender_email
    message["To"] = receiver_email

    # Delete json after assign
    #del notification_config['notification_receivers']

    # Create the plain-text and HTML version of your message
    html = json2html.convert(json=metrics_data)

    # Turn these into plain/html MIMEText objects
    # part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    message.attach(part2)

    # Create secure connection with server and send email

    # context = ssl.create_default_context()
    server = smtplib.SMTP(smtp_server, port)
    server.starttls()
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email.split(';'), message.as_string())
    server.quit()
    print("notification sent successfully ...!")
    pass
